# node-kit React Component Generator

The node-kit React component generator for the @kwaeri/node-kit platform. It's included through the @kwaeri/generators entry-point.